import {
  ChronoUnit,
  LocalDate,
  LocalDateTime,
  LocalTime,
  Period,
} from "@js-joda/core";

export function daysBetween(start: LocalDate, end: LocalDate): number {
  const differenceInDates = start.until(end, ChronoUnit.DAYS);
  return differenceInDates;
  // throw new Error("unimplemented");
}

export function afterIntervalTimes(
  start: LocalDate,
  interval: Period,
  multiplier: number
): LocalDate {
  const endDate = interval.multipliedBy(multiplier);
  return start.plus(endDate);
  // throw new Error("unimplemented");
}

export function recurringEvent(
  start: LocalDateTime,
  end: LocalDateTime,
  interval: Period,
  timeOfDay: LocalTime
): LocalDateTime[] {
  const arrayOfDates = [];
  let result = start;
  while (result.isBefore(end)) {
    if (result.toLocalTime().isAfter(timeOfDay)) {
      result = result.plus(interval);
      result = result.with(timeOfDay);
      arrayOfDates.push(result);
      result = result.plus(interval);
    } else {
      if (result.isBefore(end)) {
        result = result.with(timeOfDay);
        arrayOfDates.push(result);
      }
      result = result.plus(interval);
    }
  }
  return arrayOfDates;
  // throw new Error("unimplemented");
}
